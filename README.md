# Innovation projects for sustainability

This repository is a template of documents to be written.

 Directory structure (to be modified if project location changes):
 - General planning: https://gitlab.com/BenOrcha/projet_innovation/blob/master/general%20planning.xlsx
 - document template for the workshop 1: https://gitlab.com/BenOrcha/projet_innovation/tree/master/workshop%201%20-%20from%20idea%20to%20concept
 - document template for the workshop 2: https://gitlab.com/BenOrcha/projet_innovation/tree/master/workshop%202%20-%20from%20concept%20to%20project
 

Project indicators (to be modified if project location changes):
- who did what (RACI Matrix template): https://gitlab.com/BenOrcha/projet_innovation/blob/master/Responsibility%20assignment%20matrix%20-%20RACI%20for%20pre-studies.xlsx
- contributor's activity: https://gitlab.com/BenOrcha/projet_innovation/graphs/master
- charts: https://gitlab.com/BenOrcha/projet_innovation/graphs/master/charts


Follow this produre to create your own project based on this template :

- each team member must create a Gitlab account 
- create a public or private project under gitlab
- assign all team member to the project
- assign the project tutors to the project as reporter
- download this project template as a zip file or use the git pull command to download it
- edit the documents
- push them to your project (push git command)

For more information about git: https://git-scm.com/docs/gittutorial

